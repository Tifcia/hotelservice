import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Room {
    private int nrRoom;
    private int persons;
    private boolean bathroom;
    private boolean roomAvailable;
    private boolean isRoomClean;
    private STANDARD standard;
    private BigDecimal price;
    private String checkInDate;
    private String checkOutDate;


    List<Guest> guestList;


    public Room(int nrRoom, int persons, boolean bathroom, boolean roomAvailable, boolean isRoomClean, String checkInDate, String checkOutDate, STANDARD standard) {
        this.bathroom = bathroom;
        this.nrRoom = nrRoom;
        this.persons = persons;
        this.roomAvailable = roomAvailable;
        this.isRoomClean = isRoomClean;
        this.checkInDate = checkInDate;
        this.checkOutDate = checkOutDate;
        guestList = new ArrayList<Guest>();
    }

    public String getCheckInDate() {

        return checkInDate;
    }

    public void setCheckInDate(String checkInDate) {
        this.checkInDate = checkInDate;
    }

    public void setCheckOutDate(String checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    public String getCheckOutDate() {
        return checkOutDate;
    }


    public boolean isRoomClean() {
        return isRoomClean;
    }

    public void setRoomClean(boolean roomClean) {
        isRoomClean = roomClean;
    }

    public List<Guest> getGuestList() {
        return guestList;
    }

    public boolean isAnyGuestAdult() {

        boolean isAdult = false;
        for (Guest guest : guestList
        ) {
            if (guest.isGuestAdult()) {
                isAdult = true;
            } else {
                isAdult = false;
            }
        }
        return isAdult;
    }

    public int getNrRoom() {
        return nrRoom;
    }

    public void setNrRoom(int nrRoom) {
        this.nrRoom = nrRoom;
    }

    public int getPersons() {
        return persons;
    }

    public void setPersons(int persons) {
        this.persons = persons;
    }

    public boolean isBathroom() {
        return bathroom;
    }

    public void setBathroom(boolean bathroom) {
        this.bathroom = bathroom;
    }

    public boolean isRoomAvailable() {
        return roomAvailable;
    }

    public void setRoomAvailable(boolean roomAvailable) {
        this.roomAvailable = roomAvailable;
    }
}
