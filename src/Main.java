import java.sql.SQLOutput;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Hotel hotel = new Hotel();
        HotelService hotelService = new HotelService();
        Scanner scanner = new Scanner(System.in);
        String command = "0";
        boolean runHotelMenu = true;
        do {
            System.out.println("Chose the command: \n" +
                    "1 Show list of rooms with their status. \n" +
                    "2 Show list of available rooms. \n" +
                    "3 Book the room.\n" +
                    "4 Release the room.\n" +
                    "5 Clean room. \n" +
                    "6 Show list of clean rooms. \n" +
                    "exit Exit program");
            command = scanner.nextLine();
            switch (command) {
                case "1":
                    hotelService.showListOfRooms();
                    break;
                case "2":
                    hotelService.showAllAvailableRooms();
                    break;
                case "3":
                    hotelService.bookRoom();
                    break;
                case "4":
                    hotelService.releaseRoom();
                    break;
                case "5":
                    hotelService.cleanRoom();
                    break;
                case "6":
                    hotelService.showCleanRoomList();
                    break;
                case "7":
                    hotelService.showOccupiedRooms();
                    break;
                case "exit":
                    runHotelMenu = false;
                    break;
                default:
                    System.out.println("Command does not exist.");
            }
        } while (runHotelMenu);
        System.out.println("End of the program.");
    }
}
