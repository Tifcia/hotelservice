import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HotelService {
    Hotel hotel = new Hotel();
    Scanner sc = new Scanner(System.in);

    //Show all rooms
    public void showListOfRooms() {
        for (Room room : hotel.listOfRooms) {
            String roomIsAvailable;
            if (room.isRoomAvailable()) {
                roomIsAvailable = "available";
            } else {
                roomIsAvailable = "booked";
            }
            System.out.println("Room n#: " + room.getNrRoom() + " is " + roomIsAvailable);
        }
    }
//Show all available rooms

    public void showAllAvailableRooms() {
        for (Room room : hotel.getListOfRooms()
        ) {
            if (room.isRoomAvailable()) {
                System.out.println("Room nr " + room.getNrRoom() + " is available");
            }
        }
    }


    //Book a room; if the room is on the list and it's available set is as not available
    public void bookRoom() {

        int nrGuestRoom = 0;
//        nrGuestRoom = sc.nextInt();

        System.out.println("N# of room you would like to book: ");
        nrGuestRoom = sc.nextInt();

        for (Room room : hotel.getListOfRooms()) {
            int numberOfPersons = 0;
            String firstName = "";
            String lastName = "";
            String guestBirthDate = "";

            if (nrGuestRoom == room.getNrRoom()) {
                if (room.isRoomAvailable() && room.isRoomClean()) {
                    System.out.println("For how many people you would like to book room (1 - 6)");
                    numberOfPersons = sc.nextInt();
                    if (numberOfPersons > room.getPersons() || numberOfPersons < 1) {
                        System.out.println("Number of people is incorrect; this room is available for no more than " + room.getPersons() + " people");
                        break;
                    } else {

                        for (int i = 0; i < numberOfPersons; i++) {
                            getFirstName();
                            if (firstName != null) {
                                getLastName();
                                if (lastName != null) {
                                    getBirthDate();
                                    if (guestBirthDate != null && room.isAnyGuestAdult()) {
                                        room.guestList.add(new Guest(firstName, lastName, guestBirthDate));

                                    }

                                    System.out.println("Room n#: " + room.getNrRoom() + " is booked.");
                                    room.setRoomAvailable(false);

                                } else {
                                    System.out.println("Room can be booked by adult person");
                                }
                            }
                        }
                    }
                } else {
                    System.out.println("Room is not available.");
                }
            }
        }
    }

    public void getFirstName() {
        String firstName;
        System.out.println("Your first name: ");
        firstName = sc.next();

    }

    public void getLastName() {
        String lastName;
        System.out.println("Your last name: ");
        lastName = sc.next();
    }

    public void getBirthDate() {
        System.out.println("Your birthday in format yyyy-MM-dd: ");
        String guestBirthDate = sc.next();
    }

 public void showOccupiedRooms (){
     for (Room room : hotel.getListOfRooms()
          ) {if (room.isRoomAvailable()== false){
         System.out.println("Room nr " + room.getNrRoom() + " is occupied, date of check out: " + room.getCheckOutDate());
     }

     }
 }
    public void cleanRoom(){
        System.out.println("N# of room which should be cleaned: ");
        int nrGuestRoom = sc.nextInt();
        for (Room room : hotel.getListOfRooms()
             ) {
            if (nrGuestRoom == room.getNrRoom()){
                if (room.isRoomClean()){
                    System.out.println("Room is already clean.");
                }else {
                    room.setRoomClean(true);
                    System.out.println("Room was cleaned.");
                }
            }

        }
    }
    public void showCleanRoomList(){
        for (Room room : hotel.getListOfRooms()
        ) {
            if (room.isRoomClean()) {
                System.out.println("Room nr " + room.getNrRoom() + " is clean.");
            }
        }
    }


    //        release room
    public void releaseRoom() {
        int nrGuestRoom2 = 0;
        System.out.println("N# of room that should be released: ");
        nrGuestRoom2 = sc.nextInt();
        for (Room room : hotel.getListOfRooms()) {
            if (nrGuestRoom2 == room.getNrRoom()) {
                if (room.isRoomAvailable()) {
                    room.setRoomAvailable(true);
                    room.setRoomClean(false);
                    System.out.println("Room is released.");
                } else {
                    System.out.println("Room is now released.");
                }

            }
        }
    }
}