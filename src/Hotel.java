import java.util.ArrayList;
import java.util.List;

public class Hotel {
    List<Room> listOfRooms;


    public Hotel() {
        listOfRooms = new ArrayList<Room>();
        Room r10 = new Room(10, 2, false, true, true, null, null, STANDARD.STANDARD);
        Room r11 = new Room(11, 4, false, false, false, "2019-05-10", "2019-05-12", STANDARD.RODZINNY);
        Room r20 = new Room(20, 4, false, true, true, null, null, STANDARD.RODZINNY);
        Room r21 = new Room(21, 2, true, false, false, "2019-05-01", "2019-05-20", STANDARD.STANDARD);
        Room r22 = new Room(22, 6, true, true, true, null, null, STANDARD.STANDARD);
        Room r12 = new Room(12, 3, true, false, false, "2019-05-05", "2019-05-12", STANDARD.STANDARD);
        Room r30 = new Room(30, 1, true, true, true, null, null, STANDARD.APARTAMENT);
        Room r31 = new Room(31, 2, true, false, false, null, null, STANDARD.APARTAMENT);
        Room r32 = new Room(32, 4, true, false, false, null, null, STANDARD.RODZINNY);
        Room r33 = new Room(33, 2, true, false, false, null, null, STANDARD.STANDARD);
        Room r40 = new Room(40, 2, false, false, true, null, null, STANDARD.STANDARD);
        Room r41 = new Room(41, 2, false, true, true, null, null, STANDARD.APARTAMENT);
        Room r42 = new Room(42, 2, true, true, true, null, null, STANDARD.APARTAMENT);
        Room r50 = new Room(50, 2, true, true, true, null, null, STANDARD.APARTAMENT);
        Room r51 = new Room(51, 2, true, true, true, null, null, STANDARD.APARTAMENT);
        listOfRooms.add(r10);
        listOfRooms.add(r11);
        listOfRooms.add(r12);
        listOfRooms.add(r20);
        listOfRooms.add(r21);
        listOfRooms.add(r22);
        listOfRooms.add(r30);
        listOfRooms.add(r31);
        listOfRooms.add(r32);
        listOfRooms.add(r33);
        listOfRooms.add(r40);
        listOfRooms.add(r41);
        listOfRooms.add(r42);
        listOfRooms.add(r50);
        listOfRooms.add(r51);
    }

    public List<Room> getListOfRooms() {
        return listOfRooms;
    }

}
//    public void setListOfRooms(List<Room> listOfRooms) {
//        this.listOfRooms = listOfRooms;
//    }

