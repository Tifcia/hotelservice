import java.time.LocalDate;
import java.time.Period;
import java.util.Date;
import java.util.Scanner;

public class Guest {
    private String firstName;
    private String lastName;
    private LocalDate guestBirthDate;
    LocalDate today = LocalDate.now();


    public Guest(String firstName, String lastName, String questBirthDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.guestBirthDate = guestBirthDate;
    }

    public boolean isGuestAdult() {
        Period p = Period.between(guestBirthDate, today);
        int p2 = p.getYears();

        if (p2 >= 18) {
            return true;
        } else {
            return false;
        }
    }

}
